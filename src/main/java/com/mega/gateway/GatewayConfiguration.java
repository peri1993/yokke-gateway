package com.mega.gateway;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
@EnableResourceServer
public class GatewayConfiguration extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method studb
		http
		.csrf().disable().cors().and().formLogin().permitAll().and()
		.authorizeRequests()
		.antMatchers(HttpMethod.OPTIONS,"/oauth/**")
		.permitAll()
		.antMatchers("/**")
		//.authenticated()
		//.antMatchers("/ess-minova/**")
		.authenticated();
	}

}
