package com.mega.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
public class OAuth2Config extends AuthorizationServerConfigurerAdapter {
	private String clientid = "ess-mega";
	private String clientSecret = "B@nkMega";
	//private String privateKey = "MIIEogIBAAKCAQEA6i7LW66CQBwt9J/L68sWQUJjsSdvmi2vlHhyN9mvHi9tpMBJFlEg1EFJS5uN1oEW1dDCXozAu3tTRLt4ns9YadBJMsRxcm7H8X78xb3niMIha5fNyvqOovEFaMSKzKanr7L8IkVbwXvTmhyCsp20gJPdN7x2skOscUVyo5MPFSeHF61Mxx/16XssN+TLCBQ3KkCEQy3xUy4f3pHvBo1qTy/gdwHQtbUpSfx6xxz+IJzBr31BsBDpbLrGPZ5gOvqTq0o6ZHk6g/mV9USWxYWUmHV+YDcrvPZHkLG/0Anl/VEpsiBKkEcmSZja5sJFdUi97Z9kndXYY86mNinCAqjMmQIDAQABAoIBAENzDudtL96/Pw1CKT3U1KgfMw+HSEGj/vpIZr1nWWTcTDwzldlzrHz2AOJh/0xVCfqzkgghijF1p9oYFPnIxrJTZ4HTyskm1zfbV3qs488j+vQ+WOuplwOfX+t5EHE5XvrgFnbYe0ETJTMyGwLPIXoXHJQ9GALoMva9BfPPG3z/xGMBYImoIas4mTlJX6AOO5q8GXjOnWv7Ups5BLbjjSCDMn3hiU093k97gCL3T7thrRIPlc+BASwuTq54XdTl4NB4mvj59WeeISO3hsAd3OUPBbju/BRmMCkaBGbFvnJl9EIcnFUX1eix5drUe1N5vHsSj78il2Wau6S+0l2L/XECgYEA/ycdVbxoiz5VmckrfDYmUEmZKB1nEeFTshFJsJ9/HzrPvAa3NDo1lCdY47LheMsJzF+OTyCUSpgtTmtVQW4o0KieoMu7clhsSbOFuaKCxiutDXOdfM+jFu/TmOJXx4UAvmMLeao/TPvzqnRbwZ/NGIMpXlBQFp0JkHz+ogTdHcMCgYEA6vXa1aNBRaAaf8yzp1WspStN2L6mE4rn9L0DPXtJen3+WGTz/K6cqn2yuxEX84gBnqBSOXnbi++FdGSpaGYFmbH7rcF4M+MHE3zQPoSmg/S7RnKxl+EyXKI/q8Xm/ruF31ieCENx3U9+CpJEXP4L8tFiS0jFaSGbkU8iBfJs+nMCgYAGNbJ+Eq7MrMCylHXNeRfnnlLu6gBHLB+4VrlfZhJW9Tx3DAbLa5tANtDwAbuBEBf19GFnRZi2VAvOO8iAZijyIxhxO2QdfU9tX8QiZ3UdU5TLngO4NOl4NnAE6YVPYspI1+pAlMcjnYIppNlS1gvMy+xm0jeQxVOlQ5WC5fFntwKBgAzrwEEHXUuK5moow6hsGbZrjzhgI5nnhaH70vXKldvameYahyBaKdtmVjHbWsnVrOEgR+VJQj2LyncAIiDPm3c8Rm7juMyBq53yuzD08oNHLhVNsV58z9wwP/geRfm0LkR+BxLFQrm86V2Ddab3gYi04wbo1ZJWesue4mwZTMzpAoGAfP8KFmmmbzZKhl+Q4VlzPbalCZTnZoEAFQzxaUrV2aVT7tNkyIEOLB1SP8f6XYB0qy8QIG+Y7f8HWw4gr+gK784vKqQn0/g5pI6RImHJADDb/kVK+pcinTd3LoQwilqyytFhWSELBpjlhTTjkGj40o0EdWMyp1q/sDVTKFwVGhA=";
	//private String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6i7LW66CQBwt9J/L68sW QUJjsSdvmi2vlHhyN9mvHi9tpMBJFlEg1EFJS5uN1oEW1dDCXozAu3tTRLt4ns9Y adBJMsRxcm7H8X78xb3niMIha5fNyvqOovEFaMSKzKanr7L8IkVbwXvTmhyCsp20 gJPdN7x2skOscUVyo5MPFSeHF61Mxx/16XssN+TLCBQ3KkCEQy3xUy4f3pHvBo1q Ty/gdwHQtbUpSfx6xxz+IJzBr31BsBDpbLrGPZ5gOvqTq0o6ZHk6g/mV9USWxYWU mHV+YDcrvPZHkLG/0Anl/VEpsiBKkEcmSZja5sJFdUi97Z9kndXYY86mNinCAqjM mQIDAQAB";

	private String privateKey = "-----BEGIN RSA PRIVATE KEY-----\r\n" + 
			"MIIEogIBAAKCAQEA6i7LW66CQBwt9J/L68sWQUJjsSdvmi2vlHhyN9mvHi9tpMBJ\r\n" + 
			"FlEg1EFJS5uN1oEW1dDCXozAu3tTRLt4ns9YadBJMsRxcm7H8X78xb3niMIha5fN\r\n" + 
			"yvqOovEFaMSKzKanr7L8IkVbwXvTmhyCsp20gJPdN7x2skOscUVyo5MPFSeHF61M\r\n" + 
			"xx/16XssN+TLCBQ3KkCEQy3xUy4f3pHvBo1qTy/gdwHQtbUpSfx6xxz+IJzBr31B\r\n" + 
			"sBDpbLrGPZ5gOvqTq0o6ZHk6g/mV9USWxYWUmHV+YDcrvPZHkLG/0Anl/VEpsiBK\r\n" + 
			"kEcmSZja5sJFdUi97Z9kndXYY86mNinCAqjMmQIDAQABAoIBAENzDudtL96/Pw1C\r\n" + 
			"KT3U1KgfMw+HSEGj/vpIZr1nWWTcTDwzldlzrHz2AOJh/0xVCfqzkgghijF1p9oY\r\n" + 
			"FPnIxrJTZ4HTyskm1zfbV3qs488j+vQ+WOuplwOfX+t5EHE5XvrgFnbYe0ETJTMy\r\n" + 
			"GwLPIXoXHJQ9GALoMva9BfPPG3z/xGMBYImoIas4mTlJX6AOO5q8GXjOnWv7Ups5\r\n" + 
			"BLbjjSCDMn3hiU093k97gCL3T7thrRIPlc+BASwuTq54XdTl4NB4mvj59WeeISO3\r\n" + 
			"hsAd3OUPBbju/BRmMCkaBGbFvnJl9EIcnFUX1eix5drUe1N5vHsSj78il2Wau6S+\r\n" + 
			"0l2L/XECgYEA/ycdVbxoiz5VmckrfDYmUEmZKB1nEeFTshFJsJ9/HzrPvAa3NDo1\r\n" + 
			"lCdY47LheMsJzF+OTyCUSpgtTmtVQW4o0KieoMu7clhsSbOFuaKCxiutDXOdfM+j\r\n" + 
			"Fu/TmOJXx4UAvmMLeao/TPvzqnRbwZ/NGIMpXlBQFp0JkHz+ogTdHcMCgYEA6vXa\r\n" + 
			"1aNBRaAaf8yzp1WspStN2L6mE4rn9L0DPXtJen3+WGTz/K6cqn2yuxEX84gBnqBS\r\n" + 
			"OXnbi++FdGSpaGYFmbH7rcF4M+MHE3zQPoSmg/S7RnKxl+EyXKI/q8Xm/ruF31ie\r\n" + 
			"CENx3U9+CpJEXP4L8tFiS0jFaSGbkU8iBfJs+nMCgYAGNbJ+Eq7MrMCylHXNeRfn\r\n" + 
			"nlLu6gBHLB+4VrlfZhJW9Tx3DAbLa5tANtDwAbuBEBf19GFnRZi2VAvOO8iAZijy\r\n" + 
			"IxhxO2QdfU9tX8QiZ3UdU5TLngO4NOl4NnAE6YVPYspI1+pAlMcjnYIppNlS1gvM\r\n" + 
			"y+xm0jeQxVOlQ5WC5fFntwKBgAzrwEEHXUuK5moow6hsGbZrjzhgI5nnhaH70vXK\r\n" + 
			"ldvameYahyBaKdtmVjHbWsnVrOEgR+VJQj2LyncAIiDPm3c8Rm7juMyBq53yuzD0\r\n" + 
			"8oNHLhVNsV58z9wwP/geRfm0LkR+BxLFQrm86V2Ddab3gYi04wbo1ZJWesue4mwZ\r\n" + 
			"TMzpAoGAfP8KFmmmbzZKhl+Q4VlzPbalCZTnZoEAFQzxaUrV2aVT7tNkyIEOLB1S\r\n" + 
			"P8f6XYB0qy8QIG+Y7f8HWw4gr+gK784vKqQn0/g5pI6RImHJADDb/kVK+pcinTd3\r\n" + 
			"LoQwilqyytFhWSELBpjlhTTjkGj40o0EdWMyp1q/sDVTKFwVGhA=\r\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	private String publicKey = "-----BEGIN PUBLIC KEY-----\r\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6i7LW66CQBwt9J/L68sW\r\n" + 
			"QUJjsSdvmi2vlHhyN9mvHi9tpMBJFlEg1EFJS5uN1oEW1dDCXozAu3tTRLt4ns9Y\r\n" + 
			"adBJMsRxcm7H8X78xb3niMIha5fNyvqOovEFaMSKzKanr7L8IkVbwXvTmhyCsp20\r\n" + 
			"gJPdN7x2skOscUVyo5MPFSeHF61Mxx/16XssN+TLCBQ3KkCEQy3xUy4f3pHvBo1q\r\n" + 
			"Ty/gdwHQtbUpSfx6xxz+IJzBr31BsBDpbLrGPZ5gOvqTq0o6ZHk6g/mV9USWxYWU\r\n" + 
			"mHV+YDcrvPZHkLG/0Anl/VEpsiBKkEcmSZja5sJFdUi97Z9kndXYY86mNinCAqjM\r\n" + 
			"mQIDAQAB\r\n" + 
			"-----END PUBLIC KEY-----";
	
	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	@Bean
	public JwtAccessTokenConverter tokenEnhancer() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey(privateKey);
		converter.setVerifierKey(publicKey);
		return converter;
	}

	@Bean
	public JwtTokenStore tokenStore() {
		return new JwtTokenStore(tokenEnhancer());
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore())
				.accessTokenConverter(tokenEnhancer());
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory().withClient(clientid).secret(clientSecret).scopes("read", "write")
				.authorizedGrantTypes("password", "refresh_token").accessTokenValiditySeconds(3600)
				.refreshTokenValiditySeconds(3600);

	}

}
