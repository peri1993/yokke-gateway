package com.mega.gateway.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@EnableWebMvc
@Configuration
@Import(Swagger2.class)
public class WebConfig extends WebMvcConfigurerAdapter {

//	@Override
//	public void configureMessageConverters(List<HttpMessageConverter<?>> pConverters) {
//	    pConverters.add(RestUtils.getJSONMessageConverter());
//	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("swagger-ui.html")
//        .addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("**/**").addResourceLocations("classpath:/META-INF/resources/");

//	    registry.addResourceHandler("swagger-ui.html")
//	    .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

//	    registry
//	    .addResourceHandler("/api/browser/index.html")
//	    .addResourceLocations("classpath:/META-INF/spring-data-rest/hal-browser/index.html");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

}
